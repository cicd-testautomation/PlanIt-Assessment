package com.assessment.utilities;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReferencePaths {

	public static String resources_folderpath = System.getProperty("user.dir")+ File.separator + "Test Data" + File.separator;
	
	public static String testresources_folderpath = System.getProperty("user.dir")+ File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator;
	
	public static String report_folderpath= System.getProperty("user.dir") + File.separator+ "Test Report" + File.separator;
	
	public static String testdataexcel_filename = "AssessmentTestData.xlsx";
	public static String testdataexcel_sheetname = "Sheet1";
	
	public static String report_datefolder_format = new SimpleDateFormat("dd MMM YYYY EEE").format(new Date());
	public static String report_datetime_filename_format = new SimpleDateFormat("dd MMM YYYY EEE hh.mm a").format(new Date());
	public static String report_datefolder = report_folderpath + File.separator + report_datefolder_format;
	public static String report_datetime_filename = "AssessmentTest_" + report_datetime_filename_format +".html";
 
}