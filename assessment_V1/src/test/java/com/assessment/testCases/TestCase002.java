package com.assessment.testCases;

import org.testng.annotations.Test;

import com.assessment.pageObjects.ContactPage;
import com.assessment.pageObjects.HomePage;
import com.assessment.utilities.ExtentTestManager;
import com.aventstack.extentreports.Status;

public class TestCase002 extends BaseClass {
	
	@Test
	public void validateSuccessSubmit() {
		
		ExtentTestManager.startTest("TestCase002");
		System.out.println("Initiate TestCase002 : validateSuccessfulSubmit"); 		
		ExtentTestManager.getTest().log(Status.INFO, "Initiate TestCase002 : validateSuccessfulSubmit");
		
		HomePage hp = new HomePage(driver);
		//From the home page go to contact page
		hp.clickOnContactBar();
		ContactPage cp = new ContactPage(driver);
		//Populate mandatory fields
		cp.fillMandatoryFields(forename, email, message);
		//Click submit button
		cp.clickOnSubmitBtn();
		//Validate successful submission message
		cp.validateSuccess(driver, expectedMessage);
	}
}
