package com.assessment.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.assessment.utilities.ExtentTestManager;
import com.aventstack.extentreports.Status;

public class HomePage {

	WebDriver driver;
	
	@FindBy(xpath = "//a[text()='Contact']")
	WebElement contactBar;
	
	@FindBy(xpath = "//a[text()='Shop']")
	WebElement shopBar;
	
	@FindBy(xpath = "//a[text()='Start Shopping »']")
	WebElement startShoppingBtn;
	
	public HomePage(WebDriver driver) {
		PageFactory.initElements(driver,this); 
	}
	
	public void clickOnContactBar() {
		contactBar.click();
		ExtentTestManager.getTest().log(Status.INFO, "HomePage : Navigated to Contact Page");
	}
	
	public void clickOnShopBar() {
		shopBar.click();
		ExtentTestManager.getTest().log(Status.INFO, "Home Page : Navigated to Shop Page");
	}
}
