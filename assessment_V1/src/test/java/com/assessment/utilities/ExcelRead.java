package com.assessment.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelRead {

	FileInputStream file;
	Workbook book;
	Sheet sheet;
	Row row;
	Cell cell;
	Object[][] data;

	public Object[][] readExcelSheet(String filepath, String sheetname) throws IOException
	{
		try {
		System.out.println("Excel : Read ");
		
		file = new FileInputStream(filepath);
		book = new XSSFWorkbook(file);
		sheet = book.getSheet(sheetname);

		int rowcount = sheet.getLastRowNum();
		data = new Object[rowcount][2];	

		for(int i = 1; i < rowcount+1 ; i++)
		{
			Row currentrow = sheet.getRow(i);

			int cellcount = currentrow.getLastCellNum();

			for(int j = 0 ; j < cellcount ; j++)
			{
				Cell currentcell = currentrow.getCell(j);
				//System.out.println(currentcell.getStringCellValue());
				switch (currentcell.getCellType()) 
				{
				case STRING:
					data[i-1][j] = currentcell.getStringCellValue();
					break;

				case NUMERIC:
					data[i-1][j] = currentcell.getNumericCellValue();
					break;

				case BOOLEAN:
					data[i-1][j] = currentcell.getBooleanCellValue();
					break;

				default:
					data[i-1][j] = currentcell.getRichStringCellValue();
					break;
				}
			}
		}

		file.close();
		
		System.out.println("Excel : Content : ");
		System.out.println(Arrays.deepToString(data));
		}catch (Exception e) {
			System.out.println(e.getCause());
		}
		return data;
	}
}