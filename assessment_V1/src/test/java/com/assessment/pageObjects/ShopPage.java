package com.assessment.pageObjects;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.assessment.utilities.ExtentTestManager;
import com.aventstack.extentreports.Status;

public class ShopPage {

	WebDriver driver;
	
	@FindBy(xpath="//li[contains(@id,'product')]")
	List<WebElement> liList;
	
	@FindBy(xpath = "//a[contains(text(),'Cart')]")
	WebElement cartBar;
	
	public static Map<String, Float> ItemPriceMap = new HashMap<String, Float>();
	
	public ShopPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		try {
	        Thread.sleep(2000);
	        for (WebElement element : liList) {
	        	String itemName = element.findElement(By.tagName("h4")).getText();
	        	String itemPriceStr = element.findElement(By.tagName("span")).getText();
	        	ItemPriceMap.put(itemName, Float.parseFloat(itemPriceStr.replace("$", "")));
	        }
	        System.out.println(ItemPriceMap);
	        
	        
		}catch (Exception e) {
			e.getMessage();
		}
	}
	
	public void selectItems(WebDriver driver, String item, double unit) {
		try {
			int quantity = (int) unit;
			if (!ItemPriceMap.isEmpty()) {
				for (Entry<String, Float> mapElement : ItemPriceMap.entrySet()) {
					//System.out.println(mapElement.getKey());
					if (mapElement.getKey().equals(item)) {
						for(int i=1 ; i<=quantity ; i++) {
							for (WebElement webElement : liList) {
								//System.out.println(webElement.findElement(By.tagName("div")).getText());
								if(webElement.findElement(By.tagName("div")).getText().contains(item)) {
									Thread.sleep(1000);
									WebElement addToCartLink = webElement.findElement(By.tagName("div")).findElement(By.tagName("p")).findElement(By.tagName("a"));
									addToCartLink.click();
									System.out.println(item +" added to cart *************");
								}
							}
						} 
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		ExtentTestManager.getTest().log(Status.INFO, "Shop Page : Items Added to Cart Successfully");
	}
	
	public void goToCartPage() {
		cartBar.click();
		ExtentTestManager.getTest().log(Status.INFO, "Shop Page : Navigated to Cart Page");
	}
}
