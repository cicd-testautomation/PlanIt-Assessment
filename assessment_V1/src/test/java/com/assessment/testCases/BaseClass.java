package com.assessment.testCases;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.assessment.utilities.ExtentManager;
import com.assessment.utilities.ExtentTestManager;

public class BaseClass {

	public String baseURL = "http://jupiter.cloud.planittesting.com";
	public static WebDriver driver;
	public String forename = "Anna";
	public String email = "anna.john@gmail.com";
	public String message = "Sample Text";
	public List<String> expectedMsgList = Arrays.asList("Forename is required", "Email is required", "Message is required");
	public String expectedMessage = "Thanks Anna, we appreciate your feedback.";
	
	
	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//Drivers//chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(baseURL);
		driver.manage().window().maximize();
	}
	
	@AfterClass
	public void tearDown() {
		ExtentTestManager.endTest();
		ExtentManager.getInstance().flush();
		driver.quit();
	}
	
}
