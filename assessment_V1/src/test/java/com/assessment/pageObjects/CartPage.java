package com.assessment.pageObjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.assessment.utilities.ExtentTestManager;
import com.aventstack.extentreports.Status;

public class CartPage {

	@FindBy(xpath = "//table[@class='table table-striped cart-items']//tr")
	List<WebElement> rows;
	
	static List<Float> expSubTotalList = new ArrayList<>();
	static List<Float> actSubTotalList = new ArrayList<>();
	
	public Map<String, Float> itemPriceMap = new HashMap<String, Float>();
	
	public CartPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		itemPriceMap = ShopPage.ItemPriceMap;
	}
	
	public void validateSubTotalofItems(WebDriver driver, String item, double units) {
		
		int quantity = (int) units;
		float expSubTotal = 0;
		float actSubTotal = 0;
		try {
		if (!itemPriceMap.isEmpty() && quantity!=0) {
			for (Entry<String, Float> mapElement : itemPriceMap.entrySet()) {
				//System.out.println(mapElement.getKey());
				if(item.equals(mapElement.getKey())) {
					expSubTotal = Math.round((quantity * mapElement.getValue())*100);
					expSubTotal = expSubTotal/100;
					System.out.println("expSubTotal : " + expSubTotal);
					expSubTotalList.add(expSubTotal);
					System.out.println("expSubTotalList : " + expSubTotalList);
					break;
				}
			}
		//fetching actual sub-total 
		for (WebElement row : rows) {
			for (WebElement column : row.findElements(By.tagName("td"))) {
				if(column.getText().contains(item)) {
					String priceStr =row.findElement(By.xpath("//td[4]")).getText();
					actSubTotal = Float.parseFloat(priceStr.replace("$", ""));
					System.out.println("actSubTotal : " + actSubTotal);
					actSubTotalList.add(actSubTotal);
					System.out.println("actSubTotalList : " + actSubTotalList);
					break;
				}
			}
		}}
	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
		Assert.assertEquals(expSubTotal, actSubTotal);
		ExtentTestManager.getTest().log(Status.INFO, "Cart Page : Sub-total for Each Product is Correct");
	}
	
	
	public void validatePriceofItems(WebDriver driver, String item, double quantity) {

		float expectedPrice = 0;
		float actualPrice = 0;
		try {
			if (!itemPriceMap.isEmpty() && quantity!=0) {
				expectedPrice = itemPriceMap.get(item);
			
			//get the actual price displayed
			for (WebElement row : rows) {
				for (WebElement column : row.findElements(By.tagName("td"))) {
					if(column.getText().contains(item)) {
						String priceStr =row.findElement(By.xpath("//td[2]")).getText();
						actualPrice = Float.parseFloat(priceStr.replace("$", ""));
						break;
					}
				}
			}}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		Assert.assertEquals(expectedPrice, actualPrice);
		ExtentTestManager.getTest().log(Status.INFO, "Cart Page : Price for Each Product is Correct");
	}
	
	public void validateSumofSubTotals() {
		
		float expSum = 0;
		float actSum = 0;
		if(expSubTotalList.size()!=0 && actSubTotalList.size()!=0) {
			for (float i : expSubTotalList) {
				expSum = expSum + i;
			}
			for (float i : actSubTotalList) {
				actSum = actSum + i;
			}
		}
		
		Assert.assertEquals(expSum, actSum);
		ExtentTestManager.getTest().log(Status.INFO, "Cart Page : Sum(Subtotal) is Correct");
	}
}
