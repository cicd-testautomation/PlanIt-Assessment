package com.assessment.utilities;

import java.io.File;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentManager {

 private static ExtentReports extent;

   public static ExtentReports getInstance() {
       if (extent == null)
           createInstance();
       return extent;
   }

   //Create an extent report instance
   public static ExtentReports createInstance() {
       String fileName = getReportPath(ReferencePaths.report_datefolder) + File.separator + ReferencePaths.report_datetime_filename;
     
       ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(fileName);
       htmlReporter.config().setTestViewChartLocation(ChartLocation.BOTTOM);
       htmlReporter.config().setChartVisibilityOnOpen(true);
       htmlReporter.config().setTheme(Theme.DARK);
       htmlReporter.config().setDocumentTitle(ReferencePaths.report_datetime_filename);
       htmlReporter.config().setEncoding("utf-8");
       htmlReporter.config().setReportName(ReferencePaths.report_datetime_filename);
       htmlReporter.config().setTimeStampFormat("dd-MMM-yyyy,EEE,hh:mm a'('zzz')'");

       extent = new ExtentReports();
       extent.attachReporter(htmlReporter);
       //Set environment details
       extent.setSystemInfo("OS", System.getProperty("os.name"));
       extent.setSystemInfo("OS version", System.getProperty("os.version"));
       extent.setSystemInfo("OS arch", System.getProperty("os.arch"));
       extent.setSystemInfo("JRE version", System.getProperty("java.runtime.version"));
       extent.setSystemInfo("User name", System.getProperty("user.name"));
       extent.setSystemInfo("User directory", System.getProperty("user.dir"));

       return extent;
   }
   
   //Create the report path
   private static String getReportPath (String path) {
	   File testDirectory = new File(path);
       if (!testDirectory.exists()) {
    	   if (testDirectory.mkdir()) {
               System.out.println("Directory: " + path + " is created!" );
               return ReferencePaths.report_datefolder;
           } else {
               System.out.println("Failed to create directory: " + path);
               return System.getProperty("user.dir");
           }
       } else {
           System.out.println("Directory already exists: " + path);
       }
       return ReferencePaths.report_datefolder;
   }
}