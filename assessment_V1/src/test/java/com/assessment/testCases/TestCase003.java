package com.assessment.testCases;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.assessment.pageObjects.CartPage;
import com.assessment.pageObjects.HomePage;
import com.assessment.pageObjects.ShopPage;
import com.assessment.utilities.ExcelRead;
import com.assessment.utilities.ExtentTestManager;
import com.assessment.utilities.ReferencePaths;
import com.aventstack.extentreports.Status;

public class TestCase003 extends BaseClass {
	
	Object[][] data;
	
	@Test(priority=1, dataProvider = "dataProviderMethod")
	public void ValidatePurchase(String item, double quantity) {
		
		ExtentTestManager.startTest("TestCase003");
		System.out.println("Initiate TestCase003 : ValidatePurchase"); 		
		ExtentTestManager.getTest().log(Status.INFO, "Initiate TestCase003 : ValidatePurchase");
		
		HomePage hp = new HomePage(driver);
		//From the home page go to shop page
		hp.clickOnShopBar();
		ShopPage sp = new ShopPage(driver);
		//Buy 2 Stuffed Frog, 5 Fluffy Bunny, 3 Valentine Bear
		sp.selectItems(driver, item, quantity);
	}
	
	@Test(priority=2, dataProvider = "dataProviderMethod")
	public void ValidatePrices(String item, double quantity) {
		ShopPage sp = new ShopPage(driver);
		// Go to the cart page
		sp.goToCartPage();
		// Verify the sub-total for each product is correct
		CartPage cart = new CartPage(driver);
		cart.validateSubTotalofItems(driver, item, quantity);
		// Verify the price for each product
		cart.validatePriceofItems(driver, item, quantity);
		
	}

	@Test(priority=3)
	public void ValidateSums() {
		CartPage cart = new CartPage(driver);
		// Verify that total = sum(sub totals)
		cart.validateSumofSubTotals();
	}

	
	@DataProvider(name = "dataProviderMethod")
	public Object[][] dataProviderMethod () throws FileNotFoundException, IOException{

		ExcelRead excelobj= new ExcelRead();
		data = excelobj.readExcelSheet(ReferencePaths.testresources_folderpath+ReferencePaths.testdataexcel_filename, ReferencePaths.testdataexcel_sheetname);

		return data;
	}
}
