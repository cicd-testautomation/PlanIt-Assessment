package com.assessment.pageObjects;

import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.assessment.utilities.ExtentTestManager;
import com.aventstack.extentreports.Status;

public class ContactPage {

	WebDriver driver;
	
	@FindBy(id = "forename")
	WebElement forename;
	
	@FindBy(id = "surname")
	WebElement surname;
	
	@FindBy(id = "email")
	WebElement email;
	
	@FindBy(id = "telephone")
	WebElement telephone;
	
	@FindBy(id = "message")
	WebElement message;
	
	@FindBy(xpath = "//a[text()='Submit']")
	WebElement submitBtn;
	
	@FindBy(xpath = "//div[@class='control-group error']//span[contains(@id,'-err')]")
	List<WebElement> errorList;
	
	@FindBy(xpath = "//div[@class='alert alert-success']")
	WebElement successAlert;
	
	public ContactPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void clickOnSubmitBtn() {
		try {
			Thread.sleep(1000);
			submitBtn.click();
			ExtentTestManager.getTest().log(Status.INFO, "Contact Page : Clicked on Submit Button");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void fillMandatoryFields(String fname, String mailId, String messageTxt) {
		try {
			Thread.sleep(1000);
			forename.sendKeys(fname);
			email.sendKeys(mailId);
			message.sendKeys(messageTxt);
			ExtentTestManager.getTest().log(Status.INFO, "Contact Page : Mandatory Fileds are Populated ");
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void checkErrorMessages(WebDriver driver, List<String> expectedErrorList) {
		try {
		
	    if(!errorList.isEmpty()) {
	    for (WebElement errorElement : errorList) {
			if(expectedErrorList.contains(errorElement.getText()))
			{
				System.out.println("Contact Page : Error Message Found : "+errorElement.getText());
				ExtentTestManager.getTest().log(Status.INFO, "Contact Page : Error Message Found : "+errorElement.getText());
			}
		}}else {
			System.out.println("Contact Page : No Error Message Found !!");
			ExtentTestManager.getTest().log(Status.INFO, "Contact Page : No Error Message Found !!");
			
		}
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void validateSuccess(WebDriver driver, String expectedSuccessMsg) {
		String actualMsg = "";
		try {
			Thread.sleep(3000);
			actualMsg = successAlert.getText();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Assert.assertEquals(expectedSuccessMsg, actualMsg);
		ExtentTestManager.getTest().log(Status.INFO, "Contact Page : Success Message is Displayed : "+actualMsg);
	}
}
