package com.assessment.testCases;

import org.testng.annotations.Test;

import com.assessment.pageObjects.ContactPage;
import com.assessment.pageObjects.HomePage;
import com.assessment.utilities.ExtentTestManager;
import com.aventstack.extentreports.Status;

public class TestCase001 extends BaseClass{

	@Test
	public void ValidateErrorMessages()  {
		
		ExtentTestManager.startTest("TestCase001");
		System.out.println("Initiate TestCase001 : ValidateErrorMessages"); 		
		ExtentTestManager.getTest().log(Status.INFO, "Initiate TestCase001 : ValidateErrorMessages");
		
		HomePage hp = new HomePage(driver);
		//From the home page go to contact page
		hp.clickOnContactBar();
		//Click submit button
		ContactPage cp = new ContactPage(driver);
		cp.clickOnSubmitBtn();
		//Verify error messages
		cp.checkErrorMessages(driver, expectedMsgList);
		//Populate mandatory fields
		cp.fillMandatoryFields(forename, email, message);
		cp.clickOnSubmitBtn();
		//Validate errors are gone
		cp.checkErrorMessages(driver, expectedMsgList);
		
	}
}
